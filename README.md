# Hippu - Micro HTTP Server

# Requirements

# Getting started

## Running from source

```sh
pip install -e .
python -m hippu
```
### Installing from source

```sh
pip install .
python -m hippu
```

# Running tests

```sh
pip install .[test]
pytest
```

# Building

## Creating a wheel

```sh
$ pip install wheel
$ python setup.py bdist_wheel
```

# Issues

# Versioning

# Customizing
