from hippu.filter import Filters
from hippu.service import Context


def test_create_filters():
    def filter_a(req, res, ctx, next):
        ctx.put('a', 1)
        next(req, res, ctx)

    def filter_b(req, res, ctx, next):
        ctx.put('b', 2)
        next(req, res, ctx)

    def handler(req, res, ctx):
        ctx.put('value', 42)

    ctx = Context()

    filters = Filters([filter_a, filter_b], handler)
    filters(None, None, ctx)

    assert ctx.get('a') == 1
    assert ctx.get('b') == 2
    assert ctx.get('value') == 42
