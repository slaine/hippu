import pytest
import time
import threading
import requests

from hippu import Server
from hippu import Service
from hippu.http import Header
from hippu.http import Status


address = ('localhost', 8080)


class Handler:
    def __init__(self, response):
        self._response = response

    def __call__(self, req, res, ctx):
        res.content = self._response


def create_server():
    return Server(address)


def start_server(server):
    thread = threading.Thread(target=server.serve_forever)
    thread.start()


def http_get(path=''):
    url = "http://{}:{}/{}".format(address[0], address[1], path.lstrip('/'))
    return requests.get(url.rstrip('/'))


def test_instantiation():
    s = Server(address)
    s.server_close()


def test_create():
    s = Server.create(address)
    s.server_close()


def test_mounting_multiple_services():
    service_a = Service()
    service_a.map('GET', '/', Handler('a'))

    service_b = Service()
    service_b.map('GET', '/', Handler('b'))

    service_c = Service()
    service_c.map('GET', '/', Handler('c'))

    server = create_server()
    server.mount(service_a, '/a')
    server.mount(service_b, '/b')
    server.mount(service_c, '/c')

    start_server(server)

    try:
        response = http_get('a')
        assert response.text == 'a'
        assert response.status_code == 200

        response = http_get('b')
        assert response.text == 'b'
        assert response.status_code == 200

        response = http_get('c')
        assert response.text == 'c'
        assert response.status_code == 200
    finally:
        server.shutdown()
        server.server_close()


def test_mounting_and_unounting_service():
    service = Service()
    service.map('GET', '/', Handler('foo'))

    server = create_server()
    start_server(server)

    paths = [
        'a',
        '/a',
        '/a/',
        'a/',

        'a/b',
        '/a/b',
        'a/b/',
        '/a/b/',
        ]

    try:
        for path in paths:
            server.mount(service, path)

            response = http_get(path)
            assert response.text == 'foo'
            assert response.status_code == 200

            server.unmount(service)
    finally:
        server.shutdown()
        server.server_close()


def test_serve_request():
    def test_filter(req, res, ctx, next):
        next(req, res, ctx)

    def do_get(req, res, ctx):
        res.status_code = 200
        res.content = dict(a='b')

    def do_put(req, res, ctx):
        res.status_code = 204

    def do_post(req, res, ctx):
        res.status_code = 201

    def do_delete(req, res, ctx):
        res.status_code = 204

    def start_server(server):
        try:
            server.serve_forever()
        except KeyboardInterrupt:
            server.shutdown()

    service = Service('Test')
    service.map('GET', '/', do_get)
    service.map('PUT', '/', do_put)
    service.map('POST', '/', do_post)
    service.map('DELETE', '/', do_delete)

    service.add_filter(test_filter)

    server = Server(address)
    server.mount(service)

    threading.Thread(target=start_server, args=(server, )).start()

    url = "http://{}:{}".format(address[0], address[1])

    try:
        response = requests.get(url)
        assert response.status_code == 200
        assert response.json()['a'] == 'b'
        assert response.headers['content-type'] == str(Header.APPLICATION_JSON).lower()

        response = requests.post(url)
        assert response.status_code == 201

        response = requests.put(url)
        assert response.status_code == 204

        response = requests.delete(url)
        assert response.status_code == 204
    finally:
        server.shutdown()
        server.server_close()


# def test_add_filter():
#     def test_filter(req, res, ctx, next):
#         next(req, res, ctx)

#     server = Server(address)
#     server.add_filter(test_filter)
