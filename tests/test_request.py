import pytest
import time
import threading
import requests

from hippu import Server
from hippu import Service


address = ('localhost', 8080)


def test_serve_request():

    def do_post(req, res, ctx):
        assert req.content == b'{"a": "b"}'
        assert req.text == '{"a": "b"}'
        assert req.json['a'] == 'b'
        assert req.query['c'] == 'true'

    def start_server(server):
        try:
            server.serve_forever()
        except KeyboardInterrupt:
            server.shutdown()

    service = Service('Test')
    service.map('POST', '/', do_post)

    server = Server(address)
    server.mount(service)

    threading.Thread(target=start_server, args=(server, )).start()

    try:
        url = "http://{}:{}?c=true".format(address[0], address[1])
        response = requests.post(url, json={"a": "b"})
    finally:
        server.shutdown()
        server.server_close()
