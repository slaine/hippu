from hippu import Service
from hippu.request import Path


class Handler:
    def __init__(self, response=''):
        self._response = response

    def __call__(self, req, res, ctx):
        res.content = self._response


class Request:
    def __init__(self, method, path):
        self.path = Path(path)
        self.method = method


def test_instantiate():
    Service()
    Service('test')


def test_map():

    def collection(req, res, ctx): pass

    def resource(req, res, ctx): pass

    service = Service()

    service.map('GET', '/collection', collection)
    service.map('GET', '/collection/{id}', resource)
    service.map('GET', '/collection-with-str-id/{id:str}', resource)
    service.map('GET', '/collection-with-int-id/{id:int}', resource)
    service.map('GET', '/collection-with-float-id/{id:float}', resource)
    service.map('GET', '/collection-with-mixed-id/{id1:int}/{id2:float}/{id3:str}', resource)

    handler = service.match(Request('GET', '/collection'))
    assert handler is not None
    assert handler._handler == collection

    handler = service.match(Request('GET', '/collection/a'))
    assert handler is not None
    assert handler._handler == resource
    assert handler._context['id'] == 'a'

    handler = service.match(Request('GET', '/collection-with-str-id/1'))
    assert handler is not None
    assert handler._handler == resource
    assert handler._context['id'] == '1'

    handler = service.match(Request('GET', '/collection-with-int-id/1'))
    assert handler is not None
    assert handler._handler == resource
    assert handler._context['id'] == 1

    handler = service.match(Request('GET', '/collection-with-float-id/1.0'))
    assert handler is not None
    assert handler._handler == resource
    assert handler._context['id'] == 1.0

    handler = service.match(Request('GET', '/collection-with-mixed-id/1/2.0/test'))
    assert handler is not None
    assert handler._handler == resource
    assert handler._context['id1'] == 1
    assert handler._context['id2'] == 2.0
    assert handler._context['id3'] == 'test'


def test_map_multiple_endpoints():

    def a(req, res, ctx): pass

    def b(req, res, ctx): pass

    service = Service()

    service.map('GET', '/{id}/a', a)
    service.map('GET', '/{id}/b', b)

    handler = service.match(Request('GET', '/1/a'))
    assert handler is not None
    assert handler._handler == a

    handler = service.match(Request('GET', '/1/b'))
    assert handler is not None
    assert handler._handler == b


def test_match():
    index = Handler()
    collection = Handler()
    resource = Handler()

    service = Service()
    service.map('GET', '/', index)
    service.map('GET', '/{id}', collection)
    service.map('GET', '/{id}/resource', resource)

    handler = service.match(Request('GET', ''))
    assert handler is not None
    assert handler._handler == index

    handler = service.match(Request('GET', '/42'))
    assert handler is not None
    assert handler._handler == collection

    handler = service.match(Request('GET', '/42/resource'))
    assert handler is not None
    assert handler._handler == resource


def test_route_context():
    def handler(req, res, ctx):
        pass

    service = Service()
    service.map('GET', '/', handler, context=dict(a=1))

    r = service.match(Request('GET', '/'))
    assert r is not None
    assert r._context['a'] == 1
