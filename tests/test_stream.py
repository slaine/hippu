import io

try:
    from PIL import Image
except ImportError:
    pil_support = False
else:
    pil_support = True

import pytest

from hippu import ChunkedStream
from hippu import MJPEGStream


class Server:
    def is_stopped(self):
        return False


class Response:
    def __init__(self):
        self.server = Server()
        self.status_code = 0

        self.headers_sent = {}
        self.data = []

    def send_status(self, status):
        self.status_code = status

    def send_headers(self, headers):
        self.headers_sent.update(headers)

    def write(self, data):
        self.data.append(data)


def test_chunked_stream():
    res = Response()

    with ChunkedStream(res, 'text/plain') as stream:
        assert stream.is_open() == True

        while stream:
            stream.write("line".encode('utf-8'))
            break

    assert res.data[0] == b'4\r\n'
    assert res.data[1] == b'line'
    assert res.data[2] == b'\r\n'


@pytest.mark.skipif(not pil_support, reason="PIL not installed")
def test_mjpeg_stream():
    image = Image.new('RGB', size=(100, 100))

    res = Response()

    with MJPEGStream(res) as stream:
        assert stream.is_open() == True

        while stream:
            stream.put(image)

            with io.BytesIO() as output:
                image.save(output, format="JPEG")
                stream.write(output.getvalue())

            break
