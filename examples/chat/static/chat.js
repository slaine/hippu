function getUserID() {
    return document.getElementById('userID').value;
}

function getChannelID() {
    return document.getElementById('channelID').value;
}

function update() {
    var channelID = getChannelID();
    featchMessages(channelID, updateMessageList);

    setTimeout(function () {
        update(channelID);
    }, 50000);
}

function updateTopics() {
    featchTopics(updateTopicList);
    setTimeout(function() {
        updateTopics();
    }, 50000);
}

function submitMessage() {
    var userID = getUserID();
    var channelID = getChannelID();
    var message = document.getElementById('message').value;

    postMessage(channelID, message, userID);
    featchMessages(channelID, updateMessageList);
}

function submitTopic() {
    var topic = document.getElementById('topic').value;
    //document.getElementById('submitTopicForm').reset();
    postTopic(topic);
    featchTopics(updateTopicList);
}

function updateMessageList(messages) {
    var messageList = document.getElementById('messages');
    messageList.innerHTML = '';
    messages.forEach(function(element) {
        var date = new Date(element.date);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        hours = ("0" + hours).slice(-2);
        minutes = ("0" + minutes).slice(-2);
        var time = "<time datetime='" + date + "'>" + hours + ":" + minutes + "</time>";
        var text = linkify(element.text);
        var user = "<span class='circle' style='background-color:#" + element.userID + "'></span>"

        //console.log(text);
        messageList.innerHTML += "<li>" + time + user + text + "</li>\n";
    });
}

function updateTopicList(topics) {
    var list = document.getElementById('topics');
    list.innerHTML = '';
    topics.forEach(function(channel) {
        var url = 'http://localhost:8080/channel.html?channelID=' + channel.id;
        list.innerHTML += "<li><a href='" + url + "'>" + channel.topic + "</a></li>\n";
    });
}


function getChannelsURL() {
    return "http://localhost:8080/api/channels";
}

function getChannelURL(channelID) {
    return "http://localhost:8080/api/channels/" + channelID;
}

function getMessagesURL(channelID) {
    return getChannelURL(channelID) + "/messages";
}

function featchMessages(channelID, callback) {
    var xhr = new XMLHttpRequest();
    var url = getMessagesURL(channelID); //"url?data=" + encodeURIComponent(JSON.stringify({"email": "hey@mail.com", "password": "101010"}));
    xhr.open("GET", url, true);
    //xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            //var res = xhr.responseText;
            //alert(res);
            var json = JSON.parse(xhr.responseText);
            callback(json);
        }
    };
    xhr.send();
}

function featchTopics(callback) {
    var xhr = new XMLHttpRequest();
    var url = getChannelsURL();
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var json = JSON.parse(xhr.responseText);
            callback(json);
        }
    };
    xhr.send();
}

function postMessage(channelID, message, userID) {
    var url = "http://localhost:8080/api/channels/" + channelID + "/messages";
    var data = { 'message': message, 'userID': userID };

    doHttpPost(url, data);
}

function postTopic(topic) {
    var url = "http://localhost:8080/api/channels";
    var data = { 'topic': topic };

    doHttpPost(url, data);
}

function doHttpPost(url, data) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 201) {
            // var json = JSON.parse(xhr.responseText);
            ///console.log(json.email + ", " + json.password);
        }
    };
    xhr.send(JSON.stringify(data));
}
