import datetime
import logging
import random
from string import Template

from hippu import Server
from hippu import Service
from hippu import FileService
from hippu.filters import CORS
from hippu.response import Status

log = logging.getLogger(__name__)


class Chat:
    def __init__(self):
        self.channels = []
        self.id = str(id(self))

    def add_channel(self, topic):
        channel = Channel.create(topic)
        self.channels.append(channel)
        return channel

    def get_channel(self, channel_id):
        for c in self.channels:
            if c.id == channel_id:
                return c

        raise Exception("No channel with {0!r}".format(channel_id))


class Channel:
    MAX_MSG_COUNT = 40
    next_id = 0

    def __init__(self, id, topic):
        self.messages = []
        self.id = id
        self.topic = topic

    @classmethod
    def create(cls, topic):
        cls.next_id += 1
        return Channel(cls.next_id, topic)

    def add_message(self, text):
        message = Message.create(text)
        if len(self.messages) >= self.MAX_MSG_COUNT:
            self.messages.pop(0)
        self.messages.append(message)
        return message

    def get_message(self, id):
        for m in self.messages:
            if m.id == id:
                return m
        raise Exception("No message with id {}.".format(id))

    def remove_message(self, message):
        self.messages.remove(message)

    def __iter__(self):
        return iter([('topic', self.topic),
                     ('id', self.id)])


class Message:
    next_id = 0

    def __init__(self, id, date, text):
        self.id = id
        self.date = date
        self.text = text
        self.user_id = '000000'

    @classmethod
    def create(cls, text):
        cls.next_id += 1
        date = datetime.datetime.now()
        return Message(cls.next_id, date, text)

    def __iter__(self):
        return iter([('date', str(self.date)),
                     ('text', self.text),
                     ('id', self.id),
                     ('userID', self.user_id),
                     ])

    def __str__(self):
        return self.text


def no_such_channel(res, ctx):
    log.error("Channel with ID %r not found.", ctx.channel_id)
    log.debug(ctx)
    res.status_code = Status.BAD_REQUEST
    res.content = "No such channel.. check the channel ID!"


def get_channels(req, res, ctx):
    #
    # The below line equals to
    #   res.content = req.context.chat.channels
    #
    res.content = ctx.chat.channels


def get_channel(req, res, ctx):
    res.content = dict(ctx.channel)


def add_channel(req, res, ctx):
    topic = req.json['topic']
    ctx.chat.add_channel(topic)


def get_messages(req, res, ctx):
    res.content = ctx.channel.messages


def add_message(req, res, ctx):
    message = req.json['message']
    m = ctx.channel.add_message(message)
    m.user_id = req.json.get('userID', '000000')
    res.status_code = Status.CREATED


def change_message(req, res, ctx):
    ctx.message.text = req.json['message']


def remove_message(req, res, ctx):
    ctx.channel.remove_message(ctx['message'])


def map_resources(req, res, ctx, next):
    """ Maps reource ID to resource objects. """
    chat = ctx.chat
    channel = None

    # Channel ID is set if it's found from request query string.
    # This is done by Service object.
    if ctx.channel_id:
        try:
            channel = chat.get_channel(ctx.channel_id)
            ctx.put('channel', channel)
        except Exception as err:
            log.exception(err)
            bad_request(res, "Uups.. I can't find that channel from anywhere!")
            return

    # Channel must have been defined if message ID is set.
    if ctx.message_id:
        # No channel ID.
        if channel is None:
            bad_request(res, "Uups.. Message ID was given but channel ID is missing.")
            return

        try:
            ctx.put('message', channel.get_message(ctx.message_id))
        except Exception as err:
            log.exception(err)
            bad_request(res, "Uups.. I can't find that message from anywhere!")
            return

    next(req, res, ctx)


def bad_request(res, message):
    res.status_code = Status.BAD_REQUEST
    res.content = message


def do_template(req, res, ctx, next):
    next(req, res, ctx)

    if res.content_type == 'text/html':
        user_id = get_user_id(req, res, ctx)
        channel_id = req.query.get('channelID')

        if channel_id:
            try:
                chat = ctx['chat']
                channel = chat.get_channel(int(channel_id))
                topic = channel.topic
            except ValueError as err:
                log.warning(err)
            except Exception as err:
                log.exception(err)
            else:
                template = Template(res.text)
                res.content = template.substitute(topic=topic,
                                                  channelID=channel_id,
                                                  userID=user_id)


def get_user_id(req, res, ctx):
    cookie = req.cookie
    chat_id = ctx.chat.id

    if 'chatID' in cookie:
        if chat_id == cookie['chatID'].value:
            if 'userID' in cookie:
                return cookie['userID'].value

    res.cookie['chatID'] = ctx.chat.id
    res.cookie['chatID']['httponly'] = True
    res.cookie['chatID']['secure'] = False

    r = lambda: random.randint(0,255)
    user_id = '%02X%02X%02X' % (r(),r(),r())
    res.cookie['userID'] = user_id
    res.cookie['userID']['httponly'] = True
    res.cookie['userID']['secure'] = False

    return user_id


def main():
    # Set up logging
    logging.basicConfig(format='%(message)s', level=logging.DEBUG)

    # Server address
    address = ('localhost', 8080)

    # Create chat.
    chat = Chat()
    channel = chat.add_channel('Chit-chat')
    channel.add_message('Welcome!')

    # Create service (API).
    api = Service('API')

    # Map HTTP requests to handler.
    api.map('GET', '/channels', get_channels)
    api.map('POST', '/channels', add_channel)
    api.map('GET', '/channels/{channel_id:int}', get_channel)
    api.map('GET', '/channels/{channel_id:int}/messages', get_messages)
    api.map('POST', '/channels/{channel_id:int}/messages', add_message)

    # Put chat object in the context of sthe ervice. Context will be passed
    # to filters and handlers.
    api.update_context(chat=chat)

    # Add request filters.
    api.add_filter(map_resources)

    # Serve static files.
    app = FileService()
    app.update_context(chat=chat)
    app.add_filter(CORS)
    app.add_filter(do_template)

    # Set up server. You can also instantiate the server
    # just like "Server(address)".
    httpd = Server.create(address)
    Server.create(address)
    Server.create(address)

    # Mount api under v1 API
    httpd.mount(api, '/api')
    httpd.mount(app, '/')

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.server_close()

if __name__ == '__main__':
    main()
